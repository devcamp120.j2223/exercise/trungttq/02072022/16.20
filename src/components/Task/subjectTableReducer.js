const initialState = {
    subjectName: "",
    subjects: []
};

const subjectTableEvents = (state = initialState, action) => {
    switch (action.type) {
        case 'VALUE_HANDLER': {
            return {
                ...state,
                subjectName: action.payload.subjectName
            }
        };
        case 'ADD_SUBJECTS': {
            return {
                ...state,
                subjects: [...state.subjects, state.subjectName]
            }
        }
        default:
            return state

    }
} ;


export default subjectTableEvents;
