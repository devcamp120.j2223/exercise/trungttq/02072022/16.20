import React from "react";
import {
  Button,
  Container,
  TableContainer,
  Paper,
  Grid,
  TextField,
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";

const SubjectTable = () => {
  const dispatch = useDispatch();
  const { subjectName, subjects } = useSelector((e) => e.taskReducer);
  console.log("SubjectName", subjectName);
  console.log("subjects", subjects);

  const inpSubjectHandler = (event) => {
    dispatch({
      type: "VALUE_HANDLER",
      payload: {
        subjectName: event.target.value,
      },
    });
  };

  const addSubjectsHandler = () => {
    dispatch({
      type: "ADD_SUBJECTS",
    });
    dispatch({
      type: "VALUE_HANDLER",
      payload: {
        subjectName: "",
      },
    });
  };

  return (
    <Container>
      <Grid container mt={3}>
        <Grid xs={10} md={10} lg={10} sm={12} item>
          <TextField
            fullWidth
            placeholder="Input Task name"
            variant="outlined"
            label="Nhập nội dung dòng "
            onChange={inpSubjectHandler}
          />
        </Grid>
        <Grid xs={2} md={2} lg={2} sm={12} item textAlign={"center"}>
          <Button
            variant="contained"
            style={{ marginTop: "3px", width: "150px", height: "50px" }}
            onClick={addSubjectsHandler}
          >
            Thêm
          </Button>
        </Grid>
      </Grid>
      <br></br>
      <Grid>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 500 }} aria-label="simple table">
            <TableHead>
              <TableRow
                className="text-light"
                style={{ backgroundColor: "peachpuff" }}
              >
                <TableCell align="center" style={{ fontWeight: "bold" }}>
                  STT
                </TableCell>
                <TableCell align="center" style={{ fontWeight: "bold" }}>
                  Nội dung
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {subjects.map((element, index) => (
                <TableRow key={index}>
                  <TableCell component="th" align="center" scope="row">
                    {index + 1}
                  </TableCell>
                  <TableCell align="center">{element}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Container>
  );
};

export default SubjectTable;
