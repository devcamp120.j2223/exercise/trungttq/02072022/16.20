import {createStore, combineReducers} from 'redux'
import subjectTableEvents from '../components/Task/subjectTableReducer';

const appReducer = combineReducers({
    taskReducer: subjectTableEvents
})

const store = createStore(appReducer)

export default store;
